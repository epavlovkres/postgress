﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace postgress.model
{
	public class TeacherCourse
	{
		public int Id { set; get; }
		public Teacher Teacher { set; get; }
		public Course Course { set; get; }
	}
}
