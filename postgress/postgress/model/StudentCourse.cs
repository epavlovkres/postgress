﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace postgress.model
{
	public class StudentCourse
	{
		public int Id { set; get; }
		public Student Student { set; get; }
		public Course Course { set; get; }
	}
}
