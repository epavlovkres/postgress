﻿using System;

namespace postgress
{
	class Program
	{
		static void Main(string[] args)
		{
			var dbManager = new DbManager();
			dbManager.AddInitValues();
			dbManager.PrintTablesContent();
			Console.Write("\nInsert new teacher name: ");
			var name = Console.ReadLine();
			Console.Write("Insert new teacher lastname: ");
			var lastname = Console.ReadLine();
			dbManager.AddTeacher(name, lastname);
			dbManager.PrintTablesContent();
		}
	}
}
