﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using postgress.model;
using Microsoft.EntityFrameworkCore;

namespace postgress
{
	public class DbManager
	{
		public void AddInitValues()
		{
			using var db = new ApplicationContext();

			Teacher teacher1 = new Teacher { FirstName = "Anton", LastName = "Petrov" };
			Teacher teacher2 = new Teacher { FirstName = "Andrei", LastName = "Sinitsyn" };
			Teacher teacher3 = new Teacher { FirstName = "Alexandr", LastName = "Pavlov" };
			Teacher teacher4 = new Teacher { FirstName = "Anna", LastName = "Medvedeva" };
			Teacher teacher5 = new Teacher { FirstName = "Nikolai", LastName = "Andreev" };
			db.Teachers.AddRange(teacher1, teacher2, teacher3, teacher4, teacher5);

			Course course1 = new Course { Name = "C#" };
			Course course2 = new Course { Name = "C++" };
			Course course3 = new Course { Name = "JS" };
			Course course4 = new Course { Name = "Python" };
			Course course5 = new Course { Name = "Java" };
			db.Courses.AddRange(course1, course2, course3, course4, course5);

			Student student1 = new Student { FirstName = "Someone", LastName = "Somebodevich" };
			Student student2 = new Student { FirstName = "Evgeniy", LastName = "Pavlov" };
			Student student3 = new Student { FirstName = "Elena", LastName = "Igoreva" };
			Student student4 = new Student { FirstName = "Oleg", LastName = "Petrov" };
			Student student5 = new Student { FirstName = "Maxim", LastName = "Savin" };
			db.Students.AddRange(student1, student2, student3, student4, student5);

			db.TeacherCourses.AddRange(
				new TeacherCourse { Course = course2, Teacher = teacher1 },
				new TeacherCourse { Course = course2, Teacher = teacher3 },
				new TeacherCourse { Course = course1, Teacher = teacher4 },
				new TeacherCourse { Course = course3, Teacher = teacher2 },
				new TeacherCourse { Teacher =teacher4, Course = course4 });

			db.StudentCourses.AddRange(
				new StudentCourse { Course = course2, Student = student1 },
				new StudentCourse { Course = course5, Student = student2 },
				new StudentCourse { Course = course2, Student = student2 },
				new StudentCourse { Course = course4, Student = student3 },
				new StudentCourse { Course = course1, Student = student1 }
				);

			db.SaveChanges();
		}

		public void AddTeacher(string firstName, string lastName)
		{
			using var db = new ApplicationContext();
			db.Teachers.Add(new Teacher { FirstName = firstName, LastName = lastName });
			db.SaveChanges();
		}

		public void PrintTablesContent()
		{
			using var db = new ApplicationContext();

			var teachers = db.Teachers.ToList();
			Console.WriteLine("\nTeachers:");
			foreach (var t in teachers)
			{
				Console.WriteLine($"{t.FirstName} {t.LastName}");
			}

			var students = db.Students.ToList();
			Console.WriteLine("\nStudents:");
			foreach (var s in students)
			{
				Console.WriteLine($"{s.FirstName} {s.LastName}");
			}

			var courses = db.Courses.ToList();
			Console.WriteLine("\nCourses:");
			foreach (var c in courses)
			{
				Console.WriteLine($"{c.Name}");
			}
		}
	}
}
