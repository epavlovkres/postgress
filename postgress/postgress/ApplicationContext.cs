﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using postgress.model;

namespace postgress
{
	public class ApplicationContext: DbContext
	{
		const string ContextString = "Host=localhost;Port=5433;Username=admin;Password=123;Database=test";

		public DbSet<Teacher> Teachers { get; set; }
		public DbSet<Student> Students { get; set; }
		public DbSet<Course> Courses { get; set; }
		public DbSet<StudentCourse> StudentCourses { get; set; }
		public DbSet<TeacherCourse> TeacherCourses { get; set; }

		public ApplicationContext()
		{
			Database.EnsureCreated();
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
		{
			optionBuilder.UseNpgsql(ContextString);
		}
	}
}
